# ✨ GreMobilité ✨

## But du projet

Ce site permet de consulter les lignes présentes l'agglomération grenobloise, leurs horaires de passage et itinéraires.

## Comment le lancer ?

Version facile : allez voir <https://juliesouchet.online/TP/TP5%20MetroMobilite/> ;).

Version dev :

- récupérez le dépôt en local
- installez les dépendances avec `npm install`
- lancez le serveur de dev avec `npm start`

Tada !
![page d'accueil](accueil_gremobilite.png)



## Technos utilisées

* JQuery

* Leaflet
- Snowpack

Les données utilisées pour le contenu du site sont issues de l'API de la MétroMobilité.

## Organisation du projet

### /

Contient :

- fichiers de config :
  - npm : `package.json`, `package-lock.json`
  - snowpack (outil de build) : `snowpack.config.json`
- maquettes :
  - `maquette_desktop.jpg`
  - `maquette_mobile.jpg`
- capture d'écran : `accueil_gremobilite.png`
- License
- Ce readme

### public/

Contient :

- les pages .html :
  - `index.html` : page d'accueil, permettant de rechercher les arrêts à proximité d'une adresse fournie par l'utilisateur
  - `lignes.html` : page affichant la liste des différentes lignes, organisées par réseau
  - `horaires.html` : page dédiée à une ligne, détaillant les arrêts et prochains passages. Il y a une carte qu'on peut afficher en activant les boutons en haut à droite.
- `favicon.ico`
- `css/` : les fichiers .css compilés via Sass

### src/

Contient :

- icons/ : toutes les images (.svg, .png) utilisées sur le site. Elles sont toutes optimisées autant que possible.
- sass/ :
  - main.scss : style commun à toutes les pages du site
  - une feuille de style dédiée à chaque page
  - _colors.scss : module regroupant les différents thèmes

### doc/

 La documentation, générée automatiquement via JSDoc.

 Cf. [doc/index.html]()
