import {createBadge, addLineTo} from './icons.js';
import $ from 'jquery';
import {
  fetchAddressPos,
  encodeURL,
  fetchStopsProx,
  fetchDetailsLigne,
  fetchPatternsStop,
}
  from './fetch.js';
import './collapse.js';

$(document).ready(() => {
  const city = $('#search-city');
  const address = $('#search-address');
  const dropdown = $('#dropdown-content');
  const range = $('#search-range');
  const rangeValue = $('#search-range-value');
  const searchBtn = $('#search');
  const container = $('#results');
  // pour mémoriser une position
  let longitude = null;
  let latitude = null;

  // UTILITAIRES
  /**
   * Déchiffre un string-adresse pour en extraire les informations.
   * @param {String} ad adresse
   * @return {JSON} un objet contenant le numéro et nom de rue
   */
  const parseAddress = (ad) => {
    ad = ad.trim();
    // extraire le num de maison
    let num = ad.match(/^\d+/);
    num = num ? num[0] : '';
    // le numéro de maison :
    // '12 'rue...
    // '12bis 'rue...
    // '12 bis 'rue...
    const numRegexp = /^\d+(\s?(bis|ter)\s)?/;
    // extraire le nom de rue
    const rue = ad.replace(numRegexp, '');

    return {num: num, name: rue};
  };

  /**
   * Extrait la position géographique d'une maison, ou de toute la rue.
   * @param {JSON} street objet représentant une rue
   * @param {String} num numéro de maison (12, '30 bis'...)
   * @return {JSON} objet représentant une position : {lat, long}
   */
  const getStreetPos = (street, num) => {
    let res;
    // si la maison n'est pas précisée
    // prend la position de la rue
    if (!num) {
      const {lat, lon} = street;
      res = {lat: lat, lon: lon};
    } else {
      // cherche la maison en particulier
      const {lat, lon} = street.housenumbers[num];
      res = {lat: lat, lon: lon};
    }

    return res;
  };

  /**
   * Met à jour le contenu de l'input selon la rue sélectionnée.
   * @param {JSON} street la rue sélectionnée
   */
  const autoComplete = (street) => {
    // màj si le texte est différent
    if (address.val() != street.name) {
      address.val(street.name);
      city.val(street.postcode);
    } else {
      // mémorisation des coordonnées
      const {lat, lon} = getStreetPos(street);
      latitude = lat;
      longitude = lon;
      console.log(`Coordonnées : lon:${longitude}, lat:${latitude}`);
      // déblocage de la recherche
      searchBtn.removeAttr('disabled');
      searchBtn.focus();
    }
  };

  /**
   * Affiche des suggestions de complétion selon l'input
   * déjà entré et les rues possibles dans la liste
   * @param {Array} streetList liste d'objets décrivant des rues
   */
  const afficherSuggestions = (streetList) => {
    // vider les suggestions
    dropdown.children().remove();
    // màj de la liste de suggestions
    for (const street of streetList) {
      // créer une suggestion
      const suggestion =
        `${street.name}, ${street.city} (${street.postcode})`;
      const newItem = $(`<p class="dropdown-item" tabindex="0">
      ${suggestion}</p>`);

      newItem.appendTo(dropdown);

      // autocomplétion
      newItem.keydown((e) => {
        if (e.key == 'Enter') {
          autoComplete(street);
        }
      });
      newItem.click(() => autoComplete(street));
    }
  };

  // RECHERCHE DES ARRETS PROCHES
  /**
   * Recherche les arrêts à proximité de la position enregistrée.
   */
  const searchStopsProx = () => {
    fetchStopsProx(latitude, longitude, range.val(), true).done((stopList) => {
      if (stopList.length === 0) {
        // si aucun résultat
        $('#error').show();
      } else {
        $('#error').hide();
        for (const stop of stopList) {
          // affichage du resultat pour chaque arrêt
          const stopId = stop.id.replace(':', '-');
          const stopItem = $(`<li class="results-item"
            id="${stopId}">
            ${stop.name.toLowerCase()}
            </li>`);
          // ajout à la liste
          container.append(stopItem);

          // recherche des parcours passant à cet arrêt
          fetchPatternsStop(stop.id).done((patternList) => {
            // liste imbriquée de parcours, pour chaque arrêt
            const subList = $(`<ul></ul>`);
            subList.appendTo(stopItem);

            const duplicateCheck = [];
            for (const pattern of patternList) {
              // on extrait l'id de la ligne à partir de celui du parcours
              const lineId = pattern.id.split(':').slice(0, 2).join(':');
              // on récupère la direction
              const direction = pattern.desc;
              // pour éviter les doublons dans les 2 directions
              if (duplicateCheck.includes(lineId + direction)) {
                continue;
              }
              // si le name n'était pas dans la liste, on l'ajoute
              // et on continue le traitement
              duplicateCheck.push(lineId + direction);


              // préparation du lien
              const urlParams = 'ligne=' + encodeURL(lineId) + // ligne
                '&stop=' + encodeURL(stopId) + // arrêt
                '&back=' + (pattern.dir == 2); // direction
              // affichage du parcours
              const patItem = $(`<li></li>`);
              patItem.appendTo(subList);
              const patLink = $(`<a href="horaires.html?${urlParams}"
                class="results-link btn btn-light">
                direction ${direction}</a>`);
              patLink.appendTo(patItem);

              // recherche des infos sur la ligne
              fetchDetailsLigne(lineId).done((details) => {
                details = details[0];
                // accessibilité
                patLink.attr('title', `Horaires ligne ${details.shortName}`);
                // logo
                $(createBadge(details)).prependTo(patLink);
              });
            }
          });
        }
      }
    });
  };

  // RECHERCHE D'ADRESSE
  /**
 * Recherche une rue dans la base de donnée
 * à partir des valeurs dans le formulaire.
 * @param {Boolean} help s'il faut afficher des suggestions ou non
 */
  const searchAddress = (help = false) => {
  // lecture de l'adresse
    const ad = parseAddress(address.val());
    const postCode = city.val();

    // requête des coordonnées GPS
    fetchAddressPos(encodeURL(ad.name), postCode).done((streetList) => {
      if (help) {
      // propose différents résultats
        afficherSuggestions(streetList);
      } else if (streetList) {
      // lance une recherche automatique (avec le meilleur résultat)
        autoComplete(streetList[0]);
      } else {
      // aucun résultat
        container.text('Aucune rue à ce nom !');
      }
    });
  };

  //  ---- EVENT LISTENERS
  // recherche de suggestions
  address.on('input', () => {
    searchAddress(true);
  });
  // vraie recherche
  $('form').submit((e) => {
  // vide les résultats précédents
    container.children().remove();
    // empêche le rafraichissement de la page
    e.preventDefault();

    // si aucune position n'a été mémorisée
    // => recherche d'adresse
    if (!longitude || !latitude) {
    // eslint-disable-next-line new-cap
      $.Deferred(() => {
        searchAddress(false);
      }).done(() => {
      // recherche de lignes avec le meilleur résultat
        searchStopsProx();
      });
    } else {
    // recherche de lignes avec l'adresse sélectionnée
      searchStopsProx();
    }
  });
  range.change(() => {
    rangeValue.text(range.val());
  });


  // FAVORIS
  for (let index = 0; index < localStorage.length; index++) {
    const id = localStorage.key(index);
    const isFav = localStorage.getItem(id);
    console.log(`Favori : ${id} - ${isFav}`);

    // fetch ligne
    if (isFav == 'true') {
      fetchDetailsLigne(id).done((ligne) => {
        addLineTo(ligne[0], $('#collapse-fav'), 'fav');
      });
    }
  }
});
