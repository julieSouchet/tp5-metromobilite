import $ from 'jquery';


$('document').ready(() => {
  /**
   * Ferme (display=none) un élément pour un certain temps
   * @param {JQuery} target élément (JQuery) à fermer
   * @param {Number} delay temps avant réapparition de l'élément
   * (par défaut: 2min)
   */
  const tempClose = (target, delay = 120000) => {
    const id = target.attr('id');
    // eslint-disable-next-line max-len
    console.log(`(${new Date().toLocaleTimeString()}) Fermeture de #${id} pour ${delay}ms`);
    target.slideUp();

    setTimeout(() => {
      target.slideDown();
      // eslint-disable-next-line max-len
      console.log(`(${new Date().toLocaleTimeString()}) Réouverture de #${id} (délai terminé)`);
    }, delay);
  };

  $('#alert-close').click((e) => {
    const tar = e.delegateTarget.dataset.target;
    tempClose($(`#${tar}`));
  });

  /**
   * Cache l'icône sur lequel on clique, et montre l'autre,
   * et affiche/cache le paragraphe cible selon son état actuel.
   * @param {JQuery} target icône à cacher
   * @param {JQuery} opposite icône-soeur à montrer
   * @param {String} direction direction d'animation
   * à l'horizontale au lieu de la verticale
   */
  const toggleAnimate = (target, opposite, direction) => {
    // cache l'icône cliquée
    target.toggleClass('hidden');
    // montre l'autre icône
    target.siblings(opposite).toggleClass('hidden');

    // toggle la cible
    const collapsableId = target.data('target');
    const collapsable = $(`#${collapsableId}`);

    switch (direction) {
      case 'collapseY':
      // slide vertical jquery
        collapsable.slideToggle();
        break;
      case 'collapseX':
      // slide horizontal custom
        collapsable.toggleClass('show');
        break;
      case 'size':
      // minimiser / maximiser
        collapsable.toggleClass('minimized');
      default:
        break;
    }
  };


  $('.plus').click((e) => toggleAnimate($(e.delegateTarget),
      '.minus', 'collapseY'));
  $('.minus').click((e) => toggleAnimate($(e.delegateTarget),
      '.plus', 'collapseY'));
  $('.right').click((e) => toggleAnimate($(e.delegateTarget),
      '.left', 'collapseX'));
  $('.left').click((e) => toggleAnimate($(e.delegateTarget),
      '.right', 'collapseX'));
  $('.minimize').click((e) => toggleAnimate($(e.delegateTarget),
      '.maximize', 'size'));
  $('.maximize').click((e) => toggleAnimate($(e.delegateTarget),
      '.minimize', 'size'));
});
