import $ from 'jquery';
import {addLineTo} from './icons.js';
// TODO importer dynamiquement pour accélerer le chargement
import {fetchLignesReseau, fetchDetailsLigne} from './fetch.js';
import './collapse.js';


$('document').ready(() => {
  /*
   NB: Codes de l'API MétroMobilité

   * Réseaux :
    TRAM
    CHRONO
    PROXIMO
    FLEXO
    C38 (C38, SCOL) = transisère
    GSV (Structurantes, Secondaires, SCOL) = tougo
    TPV (Urbaines, Interurbaines, TAD, SCOL) = pays voironnais
    SNC = sncf
  */

  /*
   * Catégories de lignes :
    TAG
      Trams
      Chrono
      Proximo
      Flexo
    Tougo
    Pays Voironnais
    Transisère
    SNCF
  */


  // --- GESTION DES FAVORIS ---
  const favSection = $('#fav-section');
  const favContainer = $('#collapse-fav');
  // y a-t-il des fav ?
  for (let index = 0; index < localStorage.length; index++) {
    // récup de la clé
    const key = localStorage.key(index);
    // récup de la valeur
    const val = localStorage.getItem(key);
    // est-ce un fav ?
    if (val == 'true') {
      // l'ajoute à la liste
      fetchDetailsLigne(key).done((ligne) => {
        addLineTo(ligne[0], favContainer, 'fav');
        // montre la liste des fav
        favSection.removeClass('hidden');
      });
    }
  }

  // remplir les listes de lignes
  for (const reseau of listeReseaux) {
    // récupère le conteneur HTML correspondant
    const container = $(`#collapse-${reseau.category}`);

    // certains réseaux regroupent plusieurs types
    // on va donc fetch sur chacun
    for (const id of reseau.types) {
      fetchLignesReseau(id).done((data) => {
        // une fois la requête réussie, on en affiche le résultat
        for (const prop in data) {
          if (data.hasOwnProperty(prop)) {
            addLineTo(data[prop], container, reseau.category);
          }
        }
      }).fail((reason) => {
        // si erreur
        console.error(`Fetch réseau ${id} échoué...`);
        console.error(reason);
      });
    }
  }

  // RECHERCHE DE LIGNES : TODO
  // $('#ligneName').change(() => {
  //   const name = $('#ligneName').val().trim();

  // });
});

const listeReseaux = [
  {category: 'tram', types: ['TRAM']},
  {category: 'chrono', types: ['CHRONO']},
  {category: 'proximo', types: ['PROXIMO']},
  {category: 'flexo', types: ['FLEXO']},
  {category: 'transisere', types: ['C38']},
  {category: 'tougo', types: ['Structurantes', 'Secondaires']},
  {category: 'voiron', types: ['Urbaines', 'Interurbaines', 'TAD']},
  {category: 'sncf', types: ['SNC']},
];

// réseaux
/**
 * Renvoie la catégorie du réseau.
 * @param {String} typeReseau id du réseau sur l'API MétroMobilité
 * @return {String} la catégorie correspondante
 */
export const getCategory = (typeReseau) => {
  for (const reseau of listeReseaux) {
    if (reseau.types.includes(typeReseau)) {
      return reseau.category;
    }
  }
  // si id inconnu
  return null;
};
