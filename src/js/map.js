import {fetchPoteauxLigne, fetchGeojsonLigne} from './fetch';
import {getBgColor} from './icons.js';

const coordGrenoble = [45.18627, 5.725358];

// création de la carte
export const mymap = L.map('map').setView(coordGrenoble, 13);

// ajout des tiles
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox/streets-v11',
  tileSize: 512,
  zoomOffset: -1,
  // eslint-disable-next-line max-len
  accessToken: 'pk.eyJ1IjoianVsaWVzb3VjaGV0IiwiYSI6ImNrY3ZxZ251dDA2ZDUycm1qZ2ticjJqbm4ifQ.O-bn-YfydNtBDNxDHC3UbQ',
}).addTo(mymap);


// liste des lignes
export const listeLignesCarte = [];

// marqueur
// eslint-disable-next-line max-len
// const markerSVG = `<svg fill="${getBgcolor(line)}" viewBow="0 0 426 426"><path d="M213.3 0h-.6C139 0 79.3 59.8 79.3 133.4c0 48.2 22 111.8 65.2 189a1051 1051 0 0 0 65 101.6c1 1.2 2.3 2 3.8 2h.1c1.6 0 3-.8 4-2l64.5-109c43-80.6 64.8-141.6 64.8-181.5C346.7 59.8 286.8 0 213.3 0zM275 136.6a62 62 0 0 1-61.9 61.9 62 62 0 0 1-61.9-61.9A62 62 0 0 1 213 74.7a62 62 0 0 1 61.9 61.9z"/></svg>`;
// const marker = L.marker([poteau.lat, poteau.lon], {
//   icon: L.divIcon({
//     className: 'iconLeaflet',
//     html: markerSVG,
//   }),
// }).addTo(mymap);
/**
 * Ajoute des marqueurs sur les arrêts de la ligne.
 * @param {JSON} line détails de la ligne
 */
export const marquerArrets = (line) => {
  // liste d'arrêts
  const itemLigne = {id: line.id, arrets: []};
  listeLignesCarte.push(itemLigne);

  // fetch des coord des poteaux
  fetchPoteauxLigne(line.id).done((poteaux) => {
    for (const poteau of poteaux) {
      // pose du marqueur sur la carte
      const marker = L.marker([poteau.lat, poteau.lon])
          .addTo(mymap)
          .bindTooltip(poteau.name)
          .bindPopup(poteau.name);
      // ajout à la liste
      itemLigne.arrets.push(marker);
    }
  });
};

export const tracerLigne = (line) => {
  fetchGeojsonLigne(line.id).done((geojson) => {
    const myStyle = {
      color: '#' + getBgColor(line),
      weight: '10',
      lineCap: 'round',
    };

    L.geoJSON(geojson, {
      style: myStyle,
    }).addTo(mymap);
  });
};

