import $ from 'jquery';


/**
 * Interroge l'API MétroMobilité pour la liste des ligne
 * @param {String} id code du réseau
 * @return {Promise} une promesse pour la requête
 */
export const fetchLignesReseau = (id) => {
  // demande la liste des lignes à l'API pour un id
  return $.ajax({
    url: 'https://data.metromobilite.fr/api/routers/default/index/routes',
    dataType: 'json',
    data: {
      reseaux: id,
    },
  });
};

/**
 * Renvoie une promesse interrogeant l'API de la
 * MétroMobilité pour les détails de la ligne.
 * @param {String} id code de la ligne
 * @return {Promise} une promesse renvoyant les détails
 */
export const fetchDetailsLigne = (id) => {
  return $.ajax({
    url: 'https://data.metromobilite.fr/api/routers/default/index/routes',
    dataType: 'json',
    data: {
      codes: id,
    },
  });
};

/**
 * Renvoie une promesse interrogeant l'API de la
 * MétroMobilité pour les poteaux de la ligne.
 * @param {String} id code de la ligne
 * @return {Promise} une promesse renvoyant les poteaux
 */
export const fetchPoteauxLigne = (id) => {
  return $.ajax({
    url: `https://data.metromobilite.fr/api/routers/default/index/routes/${id}/stops`,
    dataType: 'json',
    data: {
      id: id,
    },
  });
};

/**
 * Renvoie une promesse interrogeant l'API de la
 * MétroMobilité sur les arrets de la ligne.
 * @param {String} id code de la ligne
 * @return {Promise} une promesse renvoyant les arrets
 */
export const fetchArretsLigne = (id) => {
  return $.ajax({
    url: `https://data.metromobilite.fr/api/ficheHoraires/json`,
    dataType: 'json',
    data: {
      route: id,
    },
  });
};

/**
 * Renvoie une promesse interrogeant l'API de la
 * MétroMobilité sur les horaires de l'arrêt.
 * @param {String} id code de l'arrêt (reseau:num)
 * @param {String} date date de passage
 * @return {Promise} une promesse renvoyant les horaires
 */
export const fetchHorairesArret = (id, date = '') => {
  return $.ajax({
    url: `https://data.metromobilite.fr/api/routers/default/index/stops/${id}/stoptimes/${date}`,
    dataType: 'json',
  });
};

/**
 * Renvoie l'URL de l'API de la
 * MétroMobilité sur les horaires de la ligne.
 * @param {String} id code de la ligne
 * @return {String} URL du fichier PDF les horaires
 */
export const fetchHorairesLignePDF = (id) => `https://data.metromobilite.fr/api/ficheHoraires/pdf?route=${id}`;

/**
 * Renvoie une promesse interrogeant l'API de la
 * MétroMobilité sur les arrêts à proximité d'un point GPS.
 * @param {Number} lat latitude du point
 * @param {Number} long longitude du point
 * @param {Number} dist rayon du cercle de recherche
 * @param {Boolean} circle rayon différent du défaut (300m) ?
 * @return {Promise} une promesse renvoyant les arrêts proches
 */
export const fetchStopsProx = (lat, long, dist = 300, circle = false) => {
  return $.ajax({
    url: `https://data.metromobilite.fr/api/linesNear/json`,
    dataType: 'json',
    data: {
      y: lat,
      x: long,
      dist: dist,
      details: circle,
    },
  });
};

/**
 * Renvoie une promesse interrogeant l'API de la
 * MétroMobilité sur la position GPS d'une rue.
 * @param {String} name nom de la rue (et numéro)
 * @param {Number} city code postal
 * @return {Promise} une promesse renvoyant la position de la rue
 */
export const fetchAddressPos = (name, city = '') => {
  return $.ajax({
    url: `https://data.metromobilite.fr/api/find/street/json`,
    dataType: 'json',
    data: {
      saisie: name,
      city: city,
    },
  });
};

/**
 * Renvoie une promesse interrogeant l'API de la
 * MétroMobilité sur les parcours passant par un arrêt en particulier.
 * @param {String} stopId l'id de l'arrêt
 * @return {Promise} une promesse renvoyant les parcours passant par cet arrêt
 */
export const fetchPatternsStop = (stopId) => {
  return $.ajax({
    url: `https://data.metromobilite.fr/api/routers/default/index/stops/${stopId}/patterns`,
    dataType: 'json',
  });
};

/**
 * Renvoie une promesse interrogeant l'API de la
 * MétroMobilité sur les tracés d'une ligne.
 * @param {String} lineID l'id de la ligne
 * @return {Promise} une promesse renvoyant les tracés d'une ligne
 */
export const fetchGeojsonLigne = (lineID) => {
  return $.ajax({
    url: `https://data.metromobilite.fr/api/lines/json`,
    dataType: 'json',
    data: {
      types: 'ligne',
      codes: lineID,
    },
  });
};


// ---- UTILITAIRES ----
/**
 * 'Traduit' un string de telle sorte qu'il soit utilisable dans un URL.
 * @param {String} s string à encoder
 * @return {String} un nouveau string,
 * correspondant à s avec les caractères spéciaux remplacés
 */
export const encodeURL = (s) => {
  return s.replace(/:/g, '%3A').replace(/ /g, '+');
};

// ---- LOADING ANIMATION ----
$(document).ajaxStart(() => {
  $('#loading').fadeIn();
});
$(document).ajaxStop(() => {
  $('#loading').fadeOut();
});
