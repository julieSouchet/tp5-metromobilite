/* eslint-disable no-invalid-this */
import $ from 'jquery';
import * as icons from './icons.js';
import './collapse.js';
import './map.js';
import {fetchDetailsLigne,
  fetchArretsLigne,
  fetchHorairesArret,
  fetchHorairesLignePDF,
  fetchPoteauxLigne} from './fetch.js';
import {marquerArrets, mymap, tracerLigne} from './map.js';

// TODO : ajouter un bouton pour inverser le sens

$('document').ready(() => {
  // CONSTANTES ET AUTRES VARIABLES GLOBALES
  const lineAller = $('#line-aller'); // schéma de la ligne (aller)
  const lineRetour = $('#line-retour'); // schéma de la ligne (retour)
  const errorMsg = $('#error'); // message d'erreur
  const containerAller = $('#container-aller'); // liste des arrêts
  const containerRetour = $('#container-retour'); // liste des arrêts
  // on récupère l'id de la ligne dans l'URL
  const params = new URLSearchParams(window.location.search);

  // ligne sélectionnée
  const lineId = params.get('ligne');
  /**
   * classe CSS correspondant à l'arrêt sélectionné
   */
  let currentStop = params.get('stop');
  // direction sélectionnée
  let currentDir = params.get('back') == 'true';
  // eslint-disable-next-line max-len
  console.log(`Init : ligne ${lineId} - arrêt ${currentStop} - direction ${currentDir ? 'retour' : 'aller'}`);


  // ----- UTILITAIRES -----
  /**
   * Extrait un (nouveau) tableau contenant les 2
   * terminus à partir d'une liste d'horaires (non modifiée)
   * , ou renvoie null si aucun arrêt
   * @param {Array} list liste des horaires d'une ligne
   * @return {Array} tableau contenant les 2 terminus
   */
  const getTerminus = (list) => {
    list = getStopsForwards(list);
    if (list.length) {
      // s'il y a des arrêts,
      // on récupère les terminus
      const firstTerm = list[0];
      const scdTerm = list[list.length - 1];
      return [firstTerm.stopName.toLowerCase(),
        scdTerm.stopName.toLowerCase()];
    } else {
      return null;
    }
  };

  /**
   * Extrait un (nouveau) tableau contenant les arrêts dans le sens aller,
   * à partir d'une liste d'horaires (non modifiée).
   * @return {Array} tableau contenant les arrêts dans le sens aller
   */
  const getStopsForwards = () => {
    return mainList[0].arrets;
  };

  /**
   * Extrait un (nouveau) tableau contenant les arrêts dans le sens retour,
   * à partir d'une liste d'horaires (non modifiée).
   * @return {Array} tableau contenant les arrêts dans le sens retour
   */
  const getStopsBackwards = () => {
    return mainList[1].arrets;
  };

  /**
   * Extrait la liste des horaires de passage et
   * métadonnées pour cet poteau et cette ligne,
   * à partir de la liste de tous les horaires à ce poteau.
   * @param {JSON} times liste des horaires du poteau
   * @return {JSON} data horaires à ce poteau, pour cette ligne seulement
   */
  const filterLine = (times) => {
    for (const lineTimes of times) {
      // les données ont un format :
      // "réseau:ligne:inutile"
      // on va extraire les 2 premiers pour comparer
      const array = lineTimes.pattern.id.split(':');
      const id = array[0] + ':' + array[1];
      // on cherche les données correspondant uniquement à lineId
      // donc dès qu'elles sont trouvées on va les renvoyer
      if (id == lineId) {
        return lineTimes;
      }
    }
    // si aucune ligne dans la ligne ne correspond :
    return null;
  };

  // la date
  let date = '';

  /**
   * Renvoie un tableau contenant la Date du prochain passage.
   * @param {JSON} times liste des horaires du poteau
   * @return {Array} horaires de passage, sur la forme d'un objet Date
   */
  const calculateTimes = (times) => {
    if (!times) {
      // aucun horaire
      return '';
    }
    // on extrait les données pertinentes
    return times.times.map((item) => {
      // on calcule l'heure à partir des données
      // celles-ci sont le nombre de secondes depuis minuit d'aujourd'hui
      const sec = item.scheduledArrival;
      const hours = Math.floor(sec/3600);
      // les minutes sont le reste des heures
      const min = ((sec/3600)-hours)*60;
      const res = date ? new Date(date) : new Date();
      res.setHours(hours);
      res.setMinutes(min);
      return res;
    });
  };

  const stringify = (time) => {
    let min = time.getMinutes();
    // si min ne comporte qu'un chiffre
    if (min < 10) {
      // ajoute un 0
      min = '0' + min;
    }
    return `${time.getHours()}:${min}`;
  };

  // terminus de la ligne
  let terminus;
  // liste des arrêts principale
  let mainList;
  // liste d'arrêts actuelle
  let stopList;
  // liste d'arrêts aller-retour
  let listeAller;
  let listeRetour;
  // les listes affichées sur la page
  let container;
  let line;

  /**
   * Met à jour les différentes variables globales selon
   * la direction sélectionnée.
   * @return {Boolean} s'il faut créer les listes ou pas
   */
  const selectDir = () => {
    let create = false;

    if (currentDir) {
      // retour
      line = lineRetour;
      container = containerRetour;
      // 1er affichage -> création
      if (!listeRetour) {
        listeRetour = getStopsBackwards();
        create = true;
      }
      // on affecte cette liste à l'actuelle
      stopList = listeRetour;
      // on met à jour le titre
      $('#display-title').html(`
        <span class="light-badge">${terminus[1]}</span>
        > <span class="light-badge">${terminus[0]}</span>`);
    } else {
      // aller
      line = lineAller;
      container = containerAller;
      // 1er affichage -> création
      if (!listeAller) {
        listeAller = getStopsForwards();
        create = true;
      }
      // on affecte cette liste à l'actuelle
      stopList = listeAller;
      // on met à jour le titre
      $('#display-title').html(`
        <span class="light-badge">${terminus[0]}</span>
        > <span class="light-badge">${terminus[1]}</span>`);
    }

    return create;
  };


  /**
   * Remplit la liste d'horaires pour cet arrêt, cette ligne, à la date donnée,
   * et l'affiche dans l'élément donné
   * @param {String} stopId l'id de l'arrêt
   * @param {String} date la date formatée 'AAAAMMJJ'
   * @param {JQuery} arret élément pour contenir les horaires d'arrêt
   */
  const remplirHorairesArrêt = (stopId, date, arret) => {
    // vide la liste
    arret.children('time').remove();

    fetchHorairesArret(stopId, date).done((times) => {
      // eslint-disable-next-line max-len
      // console.log(`(${new Date().toLocaleTimeString()}) fetch des horaires (${lineId} - ${stopId})`);
      times = (filterLine(times, lineId));
      // calcul des horaires depuis les données
      const nextTimes = calculateTimes(times);
      // affichage des 2 prochains horaires (si dispo)
      for (let index = 0; index < nextTimes.length; index++) {
        // pour cacher les horaires après les 2ers
        const opt = index > 1;
        const time = nextTimes[index];
        $(`<time class="time-item ${opt ? 'mobile-hidden' : ''}"
              datetime="${time.toISOString()}">
              ${stringify(time)} </time>`).appendTo(arret);
      }
    });
  };

  /**
   * Remplit la liste des arrêts et le schéma de la ligne,
   * à partir des arrêts mémorisés, et de la direction actuelle.
   * @param {Boolean} update s'il faut (re)créer les listes
   */
  const remplirListeArrêts = (update = false) => {
    // vidage des horaires
    if (update) {
      containerAller.find('.time-item').remove();
      containerRetour.find('.time-item').remove();
    }


    // toggle l'affichage des éventuelles listes précédentes
    if (line && container) {
      line.toggle();
      container.toggle();
    }

    // --- on détermine la direction
    const create = selectDir();

    // toggle l'affichage des listes actuelles
    line.toggle();
    container.toggle();

    // remplissage des listes
    if (create) {
      // affichage des arrêts
      $(stopList).each(function() {
        const name = (this.parentStation.name).toLowerCase();
        // on enlève le caractère spécial
        const stopId = this.stopId;
        const stopIdCss = stopId.replace(':', '-');

        // ---- REMPLIR SCHEMA
        // création des nouveaux items
        const radio = $(`<input type=radio name="ligne"
        value="${stopIdCss}" id="${stopIdCss}"/>`);
        const item = $(`<li>
        <label class="${stopIdCss}" for="${stopIdCss}"
        tabindex="0"> ${name} </label>
        </li>`);
        radio.prependTo(item);
        item.appendTo(line);

        // ----- SELECTION D'UN ARRET (par clic)
        const select = () => {
          // déselectionner un éventuel arrêt précédent
          if (currentStop) {
            $(`.${currentStop}`).removeClass('selected');
          }
          // màj de l'arrêt
          currentStop = radio.val();
          // sélection de l'arrêt
          $(`.${currentStop}`).addClass('selected');

          // scroll jusqu'à l'arrêt -> milieu de la fenêtre
          $('html, body').animate({
            scrollTop: $(`#arret-${currentStop}`).offset().top -
            $(window).height()/2,
          }, 500);

          // centrer la carte sur l'arrêt
          mymap.setView(getCoordArret(stopId), 17);
        };

        item.click(select);
        // TODO manipulation au clavier

        // conteneur HTML pour un arrêt dans la liste d'horaires
        const arret = $(`<li class="${stopIdCss}" id="arret-${stopIdCss}">
        <strong>${name} : </strong>
        <br/>
        </li>`);
        arret.appendTo(container);

        // présélection d'un arrêt au chargement de la page, si nécessaire
        if (currentStop && currentStop == stopIdCss) {
          radio.click();
        }

        // requête horaires
        remplirHorairesArrêt(stopId, date, arret);
      });
    }
  };


  // ----- CODE PRINCIPAL -----

  // TELECHARGEMENT : fetch le pdf via l'API
  $('#download').attr('href', fetchHorairesLignePDF(lineId));

  // on fetch les détails de la ligne via l'API
  fetchDetailsLigne(lineId).done((details) => {
    // filtre les données
    details = details[0];

    // CARTE
    marquerArrets(details);
    tracerLigne(details);

    // traitement :
    // on personnalise la page avec les détails de la ligne
    $('title').append(` - ligne ${details.shortName}`);

    const inTitle = $('#horaires-title');
    // bouton de favori
    inTitle.append(icons.createFavButton(details));
    // logo de la ligne
    inTitle.append(icons.createBadge(details));
    // nom de la ligne
    inTitle.append(details.longName.toLowerCase());

    // on récupère la liste des arrêts
    fetchArretsLigne(lineId).done((list) => {
      mainList = list;
      // eslint-disable-next-line max-len
      // console.log(`(${new Date().toLocaleTimeString()}) fetch des listes d'arrêts (${lineId})`);

      terminus = getTerminus();
      if (terminus) {
        remplirListeArrêts();

        // --- schéma ligne initialisé
        const bgLine = $('.schema').children('.bg');
        bgLine.css('background-color', `#${icons.getBgColor(details)}`);
        bgLine.css('border-color', `#${icons.getTextColor(details)}`);
      } else {
        // s'il n'y a aucun arrêt : affiche message d'erreur
        errorMsg.show();
        $('#display-times').hide();
      }
    });
  }).fail((reason) => {
    errorMsg.show();
    errorMsg.text('Récupération des données échouée : ' + reason.statusText);
    $('#display-times').hide();
  });

  // boutons d'inversion des directions
  $('#reverse').click(() => {
    // on donne la valeur opposée à la direction
    currentDir = !currentDir;

    remplirListeArrêts();
  });

  // choix de la date
  const dateInput = $('#date');
  dateInput.change(() => {
    console.log('Choix date ' + dateInput.val());
    // formatage de la date
    date = dateInput.val();
    const dateFormat = date.replace(/-/g, '');
    // récupération des arrêts : aller
    containerAller.children().each(function() {
      const stopId = $(this).attr('class').replace('-', ':');
      remplirHorairesArrêt(stopId, dateFormat, $(this));
    });
    // récupération des arrêts : retour
    containerRetour.children().each(function() {
      const stopId = $(this).attr('class').replace('-', ':');
      remplirHorairesArrêt(stopId, dateFormat, $(this));
    });
  });


  // COORDONNEES DES ARRETS
  // positions des arrêts
  const listeArrets = [];

  /**
   * Extrait les coordonnées de l'arrêt.
   * @param {String} stopId l'id de l'arrêt
   * @return {Array} [lat, lon]
   * */ 
  const getCoordArret = (stopId) => {
    for (const arret of listeArrets) {
      if (arret.id == stopId) {
        return [arret.lat, arret.lon];
      }
    }
    // si arrêt absent de la liste
    return null;
  };

  // fetch les coord
  fetchPoteauxLigne(lineId).done((poteaux) => {
    poteaux.forEach((poteau) => {
      listeArrets.push(poteau);
    });
  });
});


